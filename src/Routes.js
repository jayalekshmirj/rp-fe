import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  // ProductList as ProductListView,
  // UserList as UserListView,
  // Typography as TypographyView,
  Icons as IconsView,
  // Account as AccountView,
  // Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  Customers as CustomersView,
  Employees as EmployeesView,
  Projects as ProjectView,
  Allocation as AllocationView,
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/dashboard"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />
      <RouteWithLayout
        component={IconsView}
        exact
        layout={MainLayout}
        path="/icons"
      />
      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <RouteWithLayout
        component={CustomersView}
        exact
        layout={MainLayout}
        path="/customers"
      />
      <RouteWithLayout
        component={ProjectView}
        exact
        layout={MainLayout}
        path="/projects"
      />
      <RouteWithLayout
        component={EmployeesView}
        exact
        layout={MainLayout}
        path="/employees"
      />
      <RouteWithLayout
        component={AllocationView}
        exact
        layout={MainLayout}
        path="/allocation"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
