import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SideBar from './SideBar';
export default class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: '',
  };

  render() {
    return (
      <div className="home-app">
        <SideBar />
        <div className="page-container">{this.props.children}</div>
      </div>
    );
  }
}
