import React, { Component } from 'react';
// import { Button, SearchInput } from '../shared-components';
import project_table from './mock';
import AddProject from './components/AddProject';
import './projects.scss';

import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TablePagination,
  Button
} from '@material-ui/core';
import { SearchInput } from '../../../components';


class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      totalPage: 0,
      isAddProject: false,
      pageLimit: 10,
      editProjectData: {},
      addProjectActionType: 'add',
      confirmDeleteModal: false,
      deleteIndex: -1,
      projectsData: [],
      updatedProjectData: {},
      allCategories: [],
    };
  }
  showAddProject = () => {
    this.setState(prevState => {
      return {
        isAddProject: !prevState.isAddProject,
        addProjectActionType: 'add',
      };
    });
  };
  PaginationControl = () => {
    const { currentPage, totalPage } = this.state;
    // const { allprojects } = this.props.projects;
    return (
      <React.Fragment>
        <div className="pagination-controls">
          <span className="prev" onClick={() => this.handlePagination('prev')}></span>
          <label>
            Page{' '}
            <strong>
              {currentPage} of {totalPage}
            </strong>
          </label>
          <span className="next" onClick={() => this.handlePagination('next')}></span>
        </div>
      </React.Fragment>
    );
  };

  render() {
    const { isAddProject } = this.state;
    return (
      <div className="projects-container">
        {/* <div className="projects-header">
          <Button label="Add Project" icon="plus" onClick={() => this.showAddProject()} />
        </div> */}
        <div className="row">
        <span className="flex" />
        <Button
          label="Add Project"
          color="primary"
          variant="contained"
          onClick={() => this.showAddProject()}
        >
          Add Projects
        </Button>
      </div>
        <div className="row">
          <SearchInput 
          className="search-input" 
          placeholder="Search Customer"
          />
          {/* <this.PaginationControl /> */}
        </div>
        <CardActions className="actions">
              <TablePagination
                component="div"
                count={10} 
                page={1}
                rowsPerPage={10}
                rowsPerPageOptions={[5, 10, 25]}
              />
            </CardActions>
        <div className="project-table">
        <Card>
            <CardContent className="card-content">
          <div className="inner">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Project Name</TableCell>
                <TableCell>Project ID</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Contact Number</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {project_table.length > 0 &&
                project_table.map((project, index) => (
                  <TableRow key={project.id}>
                    <TableCell>{project.name}</TableCell>
                    <TableCell>{project.location}</TableCell>
                    <TableCell>{project.type}</TableCell>
                    <TableCell>{project.status}</TableCell>
                    <TableCell>
                        <Button variant="contained" className="btn edit">Edit</Button>
                        <Button variant="contained" className="btn btn-danger deleteButton">Delete</Button>
                      {/* <button className="btn edit">Edit</button>
                      <button className="btn delete">Delete</button> */}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          </div>
          </CardContent>
          <CardActions className="actions">
              <TablePagination
                component="div"
                count={10} 
                page={1}
                rowsPerPage={10}
                rowsPerPageOptions={[5, 10, 25]}
              />
            </CardActions>
          </Card>
          <div className="pagination">
            {/* <this.PaginationControl /> */}
          </div>
        </div>
        {isAddProject && (
          <AddProject
            show={isAddProject}
            toggleModal={e => this.setState({ isAddProject: e })}
            type={this.state.addProjectActionType}
          />
        )}
      </div>
    );
  }
}
export default Projects;
