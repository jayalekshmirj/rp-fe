import React, { Component } from 'react';
// import Modal from '../../shared-components/Modal/Modal';
import {Modal, Button} from '@material-ui/core';


class AddProject extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { toggleModal, type, show } = this.props;
    return (
      <Modal handleClick={() => toggleModal(false)}>
        <div className={`modal add-projects ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">{type === 'add' ? 'Add Project' : 'Edit Projects'}</h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <form>
                <div className="modal-body">
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Project Name*</label>
                    <div className="col-sm-8 project-input-group">
                      <input
                        type="text"
                        className="form-control add-project-input"
                        name="projectName"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Project Location*</label>
                    <div className="col-sm-8 project-input-group">
                      <input
                        type="text"
                        className="form-control add-project-input"
                        name="projectLocation"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Project Type*</label>
                    <div className="col-sm-8 project-input-group">
                      <input
                        type="text"
                        className="form-control add-project-input"
                        name="projectType"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Status</label>
                    <div className="col-sm-8 project-input-group">
                      <input
                        type="text"
                        className="form-control add-project-input"
                        name="projectStatus"
                      />
                    </div>
                  </div>
                </div>

                <div className="modal-footer">
                  <button type="button" className="btn" onClick={() => toggleModal(false)}>
                    CANCEL
                  </button>
                  <button type="submit" className="btn btn-submit">
                    {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
export default AddProject;
