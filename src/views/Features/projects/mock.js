const project_table = [
  {
    id: 1,
    name: 'Project 1',
    location: 'Location 1',
    type: 'FP',
    status: 'status',
  },
  {
    id: 12,
    name: 'Project 2',
    location: 'Location 1',
    type: 'FP',
    status: 'status',
  },
  {
    id: 13,
    name: 'Project 3',
    location: 'Location 1',
    type: 'FP',
    status: 'status',
  },
  {
    id: 14,
    name: 'Project 4',
    location: 'Location 1',
    type: 'FP',
    status: 'status',
  },
  {
    id: 15,
    name: 'Project 5',
    location: 'Location 1',
    type: 'FP',
    status: 'status',
  },
];
export default project_table;
