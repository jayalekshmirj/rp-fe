import { Projects } from './';

export default {
  path: '/dashboard/project',
  name: 'Projects',
  childRoutes: [{ path: 'projects', name: 'Projects', component: Projects, isIndex: true }],
};
