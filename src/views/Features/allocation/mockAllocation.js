const add_allocation = [
  {
    name: 'projectId',
    label: 'Project Id',
    type: 'text',
  },
  {
    name: 'project',
    label: 'Project',
    type: 'text',
  },

  {
    name: 'status',
    value: ['In Progress', 'Future', 'On Hold', 'Completed'],
    label: 'Status',
    type: 'select',
  },
  {
    name: 'required Skill',
    value: [
      'UI/UX',
      'Testing',
      'IOS',
      'Web-Angular',
      'Web-Node',
      'DevOps',
      'Web-React',
      'Java',
      'Android',
    ],
    label: 'Required Skill',
    type: 'select',
  },
  {
    name: 'projectStartDate',
    label: 'Project Start Date',
    type: 'date',
    value: '',
  },
  {
    name: 'projectEndDate',
    label: 'Project End Date',
    type: 'date',
    value: '',
  },
  {
    name: 'employeeName',
    label: 'Employee Name',
    type: 'text',
  },
  {
    name: 'joblevel',
    label: 'Job Level',
    value: ['L1', 'L2', 'L3'],
    type: 'select',
  },
  {
    name: 'allocationStartDate',
    label: 'Allocation Start Date',
    type: 'date',
    value: '',
  },
  {
    name: 'allocationEndDate',
    label: 'Allocation End Date',
    type: 'date',
    value: '',
  },
  {
    name: 'employeeLastDate',
    label: 'Employee Last Date',
    type: 'date',
  },
  {
    name: 'hiringStatus',
    label: 'Hiring Status',
    type: 'text',
  },
];
export default add_allocation;
