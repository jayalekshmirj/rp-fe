import React, { Component } from 'react';
// import { Button } from '../shared-components';
import allocation_table from './mock';
import AddAllocation from './components/AddAllocation';
import './Allocation.scss'
import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TablePagination,
  Button
} from '@material-ui/core';

import { SearchInput } from '../../../components';

class Allocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddAllocation: false,
      currentPage: 1,
      totalPage: 0,
      pageLimit: 10,
      deleteIndex: -1,
      allocationData: [],
      totalEmployees: '',
      nonBillableEmployees: '',
      employeesInBench: '',
      trainees: '',
      billableEmployees: '',
      addAllocationActionType: 'add',
      head: [],
    };
  }

  showAddAllocation = () => {
    this.setState(prevState => {
      return {
        isAddAllocation: !prevState.isAddAllocation,
        addAllocationActionType: 'add',
      };
    });
  };

  PaginationControl = () => {
    const { currentPage, totalPage } = this.state;
    return (
      <React.Fragment>
        <div className="pagination-controls">
          <span className="prev" onClick={() => this.handlePagination('prev')}></span>
          <label>
            Page{' '}
            <strong>
              {currentPage} of {totalPage}
            </strong>
          </label>
          <span className="next" onClick={() => this.handlePagination('next')}></span>
        </div>
      </React.Fragment>
    );
  };
  render() {
    const { isAddAllocation } = this.state;
    return (
      <div className="allocation-container">
        <div className="allocation-menu">
          <div className="allocation-area">
            {80}
            <div className="allocation-text">Total Employees</div>
          </div>
          <div className="allocation-area">
            {10}
            <div className="allocation-text">Non Billable</div>
          </div>
          <div className="allocation-area">
            {2}
            <div className="allocation-text">Bench</div>
          </div>
          <div className="allocation-area">
            {50}
            <div className="allocation-text">Billable</div>
          </div>
          <div className="allocation-area">
            {18}

            <div className="allocation-text">Training</div>
          </div>
        </div>
        <div className="allocation-header">
          <Button label="" icon="plus" onClick={() => this.showAddAllocation()} />
        </div>

        <div className="pagination">
          {/* <this.PaginationControl /> */}
        </div>
        <div className="allocation-table">
        <Card>
        <CardContent className="card-content">
        <Table>
            <TableHead>
              <TableRow>
                <TableCell>Employee Name</TableCell>
                <TableCell>Technology</TableCell>
                <TableCell>Experience</TableCell>
                <TableCell>Project Name</TableCell>
                <TableCell>Project Type</TableCell>
                <TableCell>Project Location</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {allocation_table.length > 0 &&
                allocation_table.map((allocation, index) => (
                  <TableRow key={allocation.id}>
                    <TableCell>{allocation.emp_name}</TableCell>
                    <TableCell>{allocation.technology}</TableCell>
                    <TableCell>{allocation.experience}</TableCell>
                    <TableCell>{allocation.project}</TableCell>
                    <TableCell>{allocation.type}</TableCell>
                    <TableCell>{allocation.location}</TableCell>
                    <TableCell>
                      <Button variant="contained" className="btn edit">Edit</Button>
                      <Button variant="contained" className="btn btn-danger">Delete</Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </CardContent>
        <CardActions className="actions">
          <TablePagination
          component="div"
          count={10} 
          page={1}
          rowsPerPage={10}
          rowsPerPageOptions={[5, 10, 25]}
        />
          </CardActions>
        </Card>

          
          <div className="pagination">
            {/* <this.PaginationControl /> */}
          </div>
        </div>
        {isAddAllocation && (
          <AddAllocation
            show={isAddAllocation}
            toggleModal={e => this.setState({ isAddAllocation: e })}
            type={this.state.addAllocationActionType}
          />
        )}
      </div>
    );
  }
}
export default Allocation;
