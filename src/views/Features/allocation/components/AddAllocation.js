import React, { Component } from 'react';
// import Modal from '../../shared-components/Modal/Modal';
import {Modal, Button} from '@material-ui/core';
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import * as moment from 'moment';
// import Moment from 'react-moment';
// import moment from 'moment';
const status = [
  { value: 'InProgress', label: 'Internal' },
  { value: 'Future', label: 'Future' },
  { value: 'On Hold', label: 'On Hold' },
  { value: 'Completed', label: 'Completed' },
];
const skills = [
  { value: 'UI/UX', label: 'UI/UX' },
  { value: 'Testing', label: 'Testing' },
  { value: 'IOS', label: 'IOS' },
  { value: 'Web-Angular', label: 'Web-Angular' },
  { value: 'Web-Node', label: 'Web-Node' },
  { value: 'DevOps', label: 'DevOps' },
  { value: 'Web-React', label: 'Web-React' },
  { value: 'Java', label: 'Java' },
  { value: 'Android', label: 'Android' },
];
const level = [
  { value: 'L1', label: 'L1' },
  { value: 'L2', label: 'L2' },
  { value: 'L3', label: 'L3' },
];

class AddAllocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectStatus: '',
      skills: '',
      projectStartDate: new Date(),
      projectEndDate: new Date(),
      allocationStartDate: new Date(),
      allocationEndDate: new Date(),
      employeeLastDate: new Date(),
      jobLevel: '',
      startDate: moment('2012-5-30', 'YYYY-M-DD'),
      endDate: moment('2015-8-31', 'YYYY-M-DD').endOf('month'),
      allMonthsInPeriod: [],
    };
  }

  onChangeProjectStatus = event => {
    this.state.projectStatus = event.value;
    this.setState({ value: event.value });
  };
  onChangeSkills = event => {
    this.state.skills = event.value;
    this.setState({ value: event.value });
  };
  onChangeJobLevel = event => {
    this.state.jobLevel = event.value;
    this.setState({ value: event.value });
  };

  onChangeProjectStartDate = projectStartDate => this.setState({ projectStartDate });
  onChangeProjectEndDate = projectEndDate => this.setState({ projectEndDate });
  onChangeAllocationStartDate = allocationStartDate => {
    this.setState({ allocationStartDate }, () => this.dateOnChange());
  };
  onChangeAllocationEndDate = allocationEndDate =>
    this.setState({ allocationEndDate }, () => this.dateOnChange());
  onChandeEmployeeLastDate = employeeLastDate => this.setState({ employeeLastDate });

  dateOnChange = () => {
    let allocationStartDate = this.state.allocationStartDate;
    let allocationEndDate = this.state.allocationEndDate;
    let startDateString = moment(allocationStartDate);
    let endDateString = moment(allocationEndDate);
    let startDate = startDateString;
    let endDate = endDateString;

    // let startDateString = '2012-5-30';
    // let endDateString = '2013-8-31';
    // let startDate = moment(startDateString, 'YYYY-M-DD');
    // let endDate = moment(endDateString, 'YYYY-M-DD').endOf('month');

    let allMonthsInPeriod = [];
    while (startDate.isBefore(endDate)) {
      allMonthsInPeriod.push(startDate.format('YYYY-MM'));
      startDate = startDate.add(1, 'month');
    }
    this.setState({ allMonthsInPeriod: allMonthsInPeriod });
    console.log(allMonthsInPeriod);
  };

  render() {
    const { toggleModal, type, show } = this.props;
    const { allMonthsInPeriod } = this.state;
    return (
      <Modal handleClick={() => toggleModal(false)}>
        <div className={`modal add-allocation ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">
                  {type === 'add' ? 'ADD ALLOCATION' : 'EDIT ALLOCATION'}
                </h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <form>
                <div className="modal-body">
                  <div className="row field">
                    <div className="col-md-6">
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Project Id</label>
                        <div className="col-sm-8 allocation-input-group">
                          <input
                            type="text"
                            className="form-control add-allocation-input"
                            name="projectName"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Project</label>
                        <div className="col-sm-8 allocation-input-group">
                          <input
                            type="text"
                            className="form-control add-allocation-input"
                            name="projectName"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Status</label>
                        <div className="col-sm-8 allocation-input-group">
                          <Select
                            placeholder="Status"
                            onChange={this.onChangeProjectStatus}
                            options={status}
                            className="add-allocation"
                            name="status"
                            type="text"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Required Skill</label>
                        <div className="col-sm-8 allocation-input-group">
                          <Select
                            type="text"
                            onChange={this.onChangeSkills}
                            options={skills}
                            placeholder="Required Skills"
                            className="add-allocation"
                            name="requiredSkills"
                          />
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Employee Last Date</label>
                        <div className="col-sm-8 allocation-input-group">
                          <div className="form-control add-allocation-input react-date-picker__wrapper">
                            <DatePicker
                              onChange={this.onChandeEmployeeLastDate}
                              value={this.state.employeeLastDate}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Allocation Start Date</label>
                        <div className="col-sm-8 allocation-input-group">
                          <div className="form-control add-allocation-input react-date-picker__wrapper">
                            <DatePicker
                              onChange={this.onChangeAllocationStartDate}
                              value={this.state.allocationStartDate}
                            />
                          </div>
                        </div>
                      </div>
                      {allMonthsInPeriod.length ? (
                        <table class="table">
                          <thead>
                            <tr>
                              <td>Months</td>
                            </tr>
                          </thead>
                          <tbody>
                            {allMonthsInPeriod.map((date, index) => (
                              <tr id={index} key={index}>
                                <td>
                                  {date} <input type="text" name="fname"></input>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      ) : (
                        ''
                      )}
                    </div>

                    <div className="col-md-6">
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Project Start Date</label>
                        <div className="col-sm-8 allocation-input-group">
                          <div className="form-control add-allocation-input react-date-picker__wrapper">
                            <DatePicker
                              onChange={this.onChangeProjectStartDate}
                              selected={this.state.projectStartDate}
                              value={this.state.projectStartDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Project End Date</label>
                        <div className="col-sm-8 allocation-input-group">
                          <div className="form-control add-allocation-input react-date-picker__wrapper">
                            <DatePicker
                              onChange={this.onChangeProjectEndDate}
                              value={this.state.projectEndDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Employee Name</label>
                        <div className="col-sm-8 allocation-input-group">
                          <input
                            type="text"
                            className="form-control add-allocation-input"
                            name="projectName"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Job Level</label>
                        <div className="col-sm-8 allocation-input-group">
                          <Select
                            type="text"
                            onChange={this.onChangeJobLevel}
                            options={level}
                            placeholder="Job Level"
                            className="add-allocation-css-1uccc91-singleValue"
                            name="jobLevel"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Hiring Status</label>
                        <div className="col-sm-8 allocation-input-group">
                          <input
                            type="text"
                            className="form-control add-allocation-input"
                            name="projectName"
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Allocation End Date</label>
                        <div className="col-sm-8 allocation-input-group">
                          <div className="form-control add-allocation-input react-date-picker__wrapper">
                            <DatePicker
                              onChange={this.onChangeAllocationEndDate}
                              value={this.state.allocationEndDate}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn btn-submit">
                    {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                  </button>
                  <button type="button" className="btn" onClick={() => toggleModal(false)}>
                    CANCEL
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
export default AddAllocation;
