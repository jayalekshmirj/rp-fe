import { Allocation } from './';

export default {
  path: '/dashboard/allocation',
  name: 'Customers',
  childRoutes: [{ path: 'allocation', name: 'Allocation', component: Allocation, isIndex: true }],
};
