import React, { Component } from 'react';
// import Modal from '../../shared-components/Modal/Modal';
import {Modal, Input, TextField} from '@material-ui/core';

import './AddEmployee.scss';
class AddEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { toggleModal, type, show } = this.props;
    return (
      <Modal open={show} handleClick={() => toggleModal(false)}>
        <div className={`modal add-employees ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">{type === 'add' ? 'Add Employee' : 'Edit Employee'}</h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <form>
                <div className="modal-body">
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Employee Name*</label>
                    <div className="col-sm-8 employee-input-group">
                      <input
                        type="text"
                        className="form-control add-employee-input"
                        name="employeeName"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Employee Id*</label>
                    <div className="col-sm-8 employee-input-group">
                      <input
                        type="text"
                        className="form-control add-employee-input"
                        name="employeeId"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Email*</label>
                    <div className="col-sm-8 employee-input-group">
                      <input
                        type="text"
                        className="form-control add-employee-input"
                        name="employeeEmail"
                      />
                    </div>
                  </div>

                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Contact Number</label>
                    <div className="col-sm-8 employee-input-group">
                      <input
                        type="text"
                        className="form-control add-employee-input"
                        name="employeeNumber"
                      />
                    </div>
                  </div>
                </div>

                <div className="modal-footer">
                  <button type="button" className="btn btn-danger" onClick={() => toggleModal(false)}>
                    CANCEL
                  </button>
                  <button type="submit" className="btn btn-submit">
                    {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
export default AddEmployee;
