import { Employees } from './';

export default {
  path: '/dashboard/employees',
  name: 'Employees',
  childRoutes: [{ path: 'employees', name: 'Employees', component: Employees, isIndex: true }],
};
