const employee_table = [
  {
    id: 1,
    name: 'Name 1',
    emp_id: 101,
    email: 'name1@gmail.com',
    contact_number: '7845125874',
  },
  {
    id: 12,
    name: 'Name 2',
    emp_id: 101,
    email: 'name1@gmail.com',
    contact_number: '7845125874',
  },
  {
    id: 13,
    name: 'Name 3',
    emp_id: 101,
    email: 'name1@gmail.com',
    contact_number: '7845125874',
  },
  {
    id: 14,
    name: 'Name 4',
    emp_id: 101,
    email: 'name1@gmail.com',
    contact_number: '7845125874',
  },
  {
    id: 15,
    name: 'Name 5',
    emp_id: 101,
    email: 'name1@gmail.com',
    contact_number: '7845125874',
  },
];
export default employee_table;
