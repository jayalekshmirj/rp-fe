import React, { Component } from 'react';
// import { Button, SearchInput } from '../shared-components';
import employee_table from './mock';
import AddEmployee from './components/AddEmployee';
import './employees.scss'

import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TablePagination,
  Button
} from '@material-ui/core';
import { SearchInput } from '../../../components';


class Employees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      totalPage: 0,
      isAddEmployee: false,
      pageLimit: 10,
      editEmployeeData: {},
      addEmployeeActionType: 'add',
      confirmDeleteModal: false,
      deleteIndex: -1,
      employeesData: [],
      updatedEmployeeData: {},
      allCategories: [],
    };
  }

  showAddEmployee = () => {
    this.setState(prevState => {
      return {
        isAddEmployee: !prevState.isAddEmployee,
        addEmployeeActionType: 'add',
      };
    });
  };

  PaginationControl = () => {
    const { currentPage, totalPage } = this.state;
    return (
      <React.Fragment>
        <div className="pagination-controls">
          <span className="prev" onClick={() => this.handlePagination('prev')}></span>
          <label>
            Page{' '}
            <strong>
              {currentPage} of {totalPage}
            </strong>
          </label>
          <span className="next" onClick={() => this.handlePagination('next')}></span>
        </div>
      </React.Fragment>
    );
  };
  render() {
    const { isAddEmployee } = this.state;
    return (
      <div className="employees-container">
        {/* <div className="employees-header">
          <Button label="Add Employee" icon="plus" onClick={() => this.showAddEmployee()} />
        </div> */}
        <div className="row">
        <span className="flex"/>
        <Button
          color="primary"
          variant="contained"
          onClick={() => this.showAddEmployee()}
        >
          Add Employee
        </Button>
      </div>
        <div className="row">
          <SearchInput 
          placeholder="Search Employee"
          className="search-input"/>
          {/* <this.PaginationControl /> */}
        </div>
        <CardActions className="actions">
          <TablePagination
          component="div"
          count={10} 
          page={1}
          rowsPerPage={10}
          rowsPerPageOptions={[5, 10, 25]}
        />
          </CardActions>
        <div className="employee-table">
        <Card>
            <CardContent className="card-content">
              <div className="inner">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Employee Name</TableCell>
                <TableCell>Employee ID</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Contact Number</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {employee_table.length > 0 &&
                employee_table.map((employee, index) => (
                  <TableRow key={employee.id}>
                    <TableCell>{employee.name}</TableCell>
                    <TableCell>{employee.emp_id}</TableCell>
                    <TableCell>{employee.email}</TableCell>
                    <TableCell>{employee.contact_number}</TableCell>
                    <TableCell>
                        <Button variant="contained" className="btn edit">Edit</Button>
                        <Button variant="contained" className="btn btn-danger deleteButton">Delete</Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          </div>
          </CardContent>
          <CardActions className="actions">
          <TablePagination
          component="div"
          count={10} 
          page={1}
          rowsPerPage={10}
          rowsPerPageOptions={[5, 10, 25]}
        />
          </CardActions>
          </Card>
          <div className="pagination">
            {/* <this.PaginationControl /> */}
          </div>
        </div>
        {isAddEmployee && (
          <AddEmployee
            show={isAddEmployee}
            toggleModal={e => this.setState({ isAddEmployee: e })}
            type={this.state.addEmployeeActionType}
          />
        )}
      </div>
    );
  }
}
export default Employees;
