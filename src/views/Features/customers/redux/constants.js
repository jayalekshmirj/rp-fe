export const RETRIEVE_CUSTOMERS_BEGIN = 'RETRIEVE_CUSTOMERS_BEGIN';
export const RETRIEVE_CUSTOMERS_SUCCESS = 'RETRIEVE_CUSTOMERS_SUCCESS';
export const RETRIEVE_CUSTOMERS_FAILURE = 'RETRIEVE_CUSTOMERS_FAILURE';
export const CREATE_CUSTOMERS_BEGIN = 'CREATE_CUSTOMERS_BEGIN';
export const CREATE_CUSTOMERS_SUCCESS = 'CREATE_CUSTOMERS_SUCCESS';
export const CREATE_CUSTOMERS_FAILURE = 'CREATE_CUSTOMERS_FAILURE';
