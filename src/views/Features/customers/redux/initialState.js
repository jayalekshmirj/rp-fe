const initialState = {
  retrieveCustomersBegin: false,
  retrieveCustomersFailure: false,
  allCustomers: [],
  totalElements: '',
  createCustomersBegin: false,
  createCustomersFailure: false,
  totalPageCount: 0,
};

export default initialState;
