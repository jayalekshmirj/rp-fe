import {
  RETRIEVE_CUSTOMERS_BEGIN,
  RETRIEVE_CUSTOMERS_SUCCESS,
  RETRIEVE_CUSTOMERS_FAILURE,
} from './constants';
import http from '../../../../common/http';
import { apiEndPoints } from '../../../../common/globalConstants';

export function retrieveCustomers(props = {}) {
  return dispatch => {
    dispatch({ type: RETRIEVE_CUSTOMERS_BEGIN });
    console.log(props, 'props in retrieveCustomer page');

    let url = `${apiEndPoints.customers.RETRIEVE_CUSTOMERS}?page=${props.page}&size=${props.pageLimit}`;
    url = props.name ? url + `&name=${props.name}` : url;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_CUSTOMERS_SUCCESS,
            data: res,
          });
          console.log(res, 'response');
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_CUSTOMERS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_CUSTOMERS_BEGIN:
      return {
        ...state,
        retrieveCustomersBegin: true,
        retrieveCustomersFailure: false,
      };
    case RETRIEVE_CUSTOMERS_SUCCESS:
      console.log(action.data.headers['x-total-count'], 'data');
      // console.log((action.data.headers.get('X-Total-Count'), 'count'));

      return {
        ...state,
        retrieveCustomersBegin: false,
        allCustomers: action.data.data,
        totalElements: action.data.headers['x-total-count'],
      };
    case RETRIEVE_CUSTOMERS_FAILURE:
      return {
        retrieveCustomersBegin: false,
        retrieveCustomersFailure: true,
        ...state,
      };

    default:
      return state;
  }
}
