import {
  CREATE_CUSTOMERS_BEGIN,
  CREATE_CUSTOMERS_SUCCESS,
  CREATE_CUSTOMERS_FAILURE,
} from './constants';
import http from '../../../../common/http';
import { apiEndPoints } from '../../../../common/globalConstants';
import { retrieveCustomers } from './retrieveCustomers';

export function createCustomers(props = {}) {
  return dispatch => {
    dispatch({
      type: CREATE_CUSTOMERS_BEGIN,
    });
    const url = `${apiEndPoints.customers.CREATE_CUSTOMERS}`;

    const promise = new Promise((resolve, reject) => {
      const doRequest = http.post(url, props);
      doRequest.then(
        res => {
          dispatch({
            type: CREATE_CUSTOMERS_SUCCESS,
            data: res.data,
          });
          retrieveCustomers();
          resolve(res);
        },
        err => {
          dispatch({
            type: CREATE_CUSTOMERS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}
export function reducer(state, action) {
  switch (action.type) {
    case CREATE_CUSTOMERS_BEGIN:
      return {
        ...state,
        createCustomersBegin: true,
        createCustomersFailure: false,
        createCustomersSuccess: false,
      };
    case CREATE_CUSTOMERS_SUCCESS:
      return {
        ...state,
        createCustomersBegin: false,
        createCustomersSuccess: true,
        allCustomers: [...state.allCustomers, ...action.data],
        // createdProjectId: action.data.data.projectId[0],
      };
    case CREATE_CUSTOMERS_FAILURE:
      return {
        ...state,
        createCustomersBegin: false,
        createCustomersFailure: true,
      };

    default:
      return state;
  }
}
