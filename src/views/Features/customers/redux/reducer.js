import initialState from './initialState';
import { reducer as retrieveCustomers } from './retrieveCustomers';
import { reducer as createCustomers } from './createCustomers';

const reducers = [retrieveCustomers, createCustomers];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case 'RESET_CUSTOMERS_STATE':
      return {
        ...state,
        ...initialState,
      };
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
