import React, { Component } from 'react';
import {Modal, Button} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Select from 'react-select';
import '../../../../../../node_modules/bootstrap/scss/bootstrap.scss'
import './AddCustomer.scss'

const status = [
  { code: 'ACTIVE', label: 'ACTIVE', id: 1 },
  { code: 'INACTIVE', label: 'INACTIVE', id: 2 },
];
const geography = [
  { code: 'US', label: 'US', id: 1 },
  { code: 'SG', label: 'SG', id:2 },
  { code: 'AUS', label: 'AUS', id: 3 },
  { code: 'OTHER', label: 'OTHER', id: 4 },
];

const useStyles = (theme => ({
  root: {
    
      marginLeft: theme.spacing(1),
  },
}));

class AddCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerData: {
        name: '',
        poc: '',
        email: '',
        geography: '',
        status: '',
      },
    };
  }

  formSubmit = event => {
    event.preventDefault();
    const { customerData } = this.state;
    const { createCustomers, toggleModal,retrieveCustomers } = this.props;
    console.log(this.state.customerData, 'cus data'); 
    const customerData1 = {
      ...customerData,
      status:customerData.status.label
    }
    createCustomers(customerData1);
    toggleModal(false);
  };

  changeHandlerCustomerName = event => {
    this.state.customerData.name = event.target.value;
    this.setState({ value: event.target.value });
  };
  changeHandlerCustomerPoc = event => {
    this.state.customerData.poc = event.target.value;
    this.setState({ value: event.target.value });
  };
  changeHandlerCustomerEmail = event => {
    this.state.customerData.email = event.target.value;
    this.setState({ value: event.target.value });
  };
  changeHandlerCustomerCode = event => {
    this.state.customerData.geography = event;
    this.setState({ code: event.code });
  };
  onChangeCustomerStatus = event => {
    this.state.customerData.status = event;
    this.status = event;
    this.setState({ code: event.code });
  };

  componentDidMount() {
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
    if (this.props.type === 'edit') {
      this.setState({
        customerData: JSON.parse(JSON.stringify(this.props.editCustomerData)),
      });
      const customerData = {
        ...this.props.editCustomerData,
        status: {
          label: this.props.editCustomerData.status,
        },
        geography: { label: this.props.editCustomerData.geography.code,
        id:this.props.editCustomerData.geography.id,
        code:this.props.editCustomerData.geography.code 
      },
      };
      this.setState({ customerData: { ...customerData } });
      this.status = customerData.status;
    }
  }

  render() {
    const { toggleModal, type, show } = this.props;
    let customerData = this.state;
    console.log(customerData, 'customerData');
    const { classes } = this.props;

    return (
      <Modal open={show} handleClick={() => toggleModal(false)}>
        <div className={`modal add-customers ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">{type === 'add' ? 'ADD CUSTOMER' : 'EDIT CUSTOMER'}</h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <form onSubmit={e => this.formSubmit(e)}>
                <div className="modal-body">
                  <div className="row field">
                    <div className="col-md-6">
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Customer Name</label>
                        <div className="col-sm-8 customer-input-group">
                          <input
                            type="text"
                            className="form-control add-customer-input"
                            // name="projectName"
                            onChange={this.changeHandlerCustomerName}
                            value={this.state.customerData.name}
                          />
                        </div>
                      </div>

                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Email</label>
                        <div className="col-sm-8 customer-input-group">
                          <input
                            type="text"
                            className="form-control add-customer-input"
                            // name="email"
                            onChange={this.changeHandlerCustomerEmail}
                            value={this.state.customerData.email}
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Status</label>
                        <div className="col-sm-8 customer-input-group">
                          <Select
                            placeholder="Status"
                            onChange={this.onChangeCustomerStatus}
                            options={status}
                            className="add-customer"
                            name="status"
                            type="Status"
                            value={this.status}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">POC</label>
                        <div className="col-sm-8 customer-input-group">
                          <input
                            type="text"
                            className="form-control add-customer-input"
                            // name="poc"
                            onChange={this.changeHandlerCustomerPoc}
                            value={this.state.customerData.poc}
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Geography Code</label>
                        <div className="col-sm-8 customer-input-group">
                          <Select
                            placeholder="geography"
                            onChange={this.changeHandlerCustomerCode}
                            options={geography}
                            className="add-customer"
                            name="geography"
                            value={this.state.customerData.geography}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <Button type="submit" color="primary" variant="contained" className={classes.root}>
                    {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                  </Button>
                  <Button color="primary" variant="contained" className={classes.root} onClick={() => toggleModal(false)}>
                    CANCEL
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}
// export default AddCustomer;
export default withStyles(useStyles)(AddCustomer)
