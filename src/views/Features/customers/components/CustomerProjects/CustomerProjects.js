import React, { Component } from 'react';


class RowContent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
    render(){
      
      let jsxhtml ='';
      
      if (this.props.open) {
        jsxhtml = (<div className="content open">
          
          {this.props.children}
        </div>);
      }
      
      return (
        <div>
        {jsxhtml}
          </div>
      )
    }
  };

  export default RowContent