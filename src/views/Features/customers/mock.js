const customer_table = [
  {
    id: 1,
    name: 'Name 1',
    project: 'Project 1',
    email: 'name1@gmail.com',
    location: 'Location',
  },
  {
    id: 12,
    name: 'Name 2',
    project: 'Project 1',
    email: 'name1@gmail.com',
    location: 'Location',
  },
  {
    id: 13,
    name: 'Name 3',
    project: 'Project 1',
    email: 'name1@gmail.com',
    location: 'Location',
  },
  {
    id: 14,
    name: 'Name 4',
    project: 'Project 1',
    email: 'name1@gmail.com',
    location: 'Location',
  },
  {
    id: 15,
    name: 'Name 5',
    project: 'Project 1',
    email: 'name1@gmail.com',
    location: 'Location',
  },
];
export default customer_table;

const cus = [ {
  "id" : 1755,
  "name" : "QA Test",
  "poc" : "poc",
  "email" : "test@gmail.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 3,
    "code" : "AUS"
  }
}, {
  "id" : 7,
  "name" : "Exacta Process Platforms Pte. Ltd.",
  "poc" : "Rafael Montes",
  "email" : "rmm@exacta-platform.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 8,
  "name" : "ExpatChoice",
  "poc" : "John Gordon",
  "email" : "john@expatchoice.asia",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 9,
  "name" : "Food Concepts Group Singapore Pte Ltd",
  "poc" : "Vadim Korob",
  "email" : "vadim.korob@loyaltystudios.io",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 10,
  "name" : "IM Nuper Technology.....",
  "poc" : "Mohammed Farruh",
  "email" : "mfarruh@yahoo.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 11,
  "name" : "Kuehne & Nagel Limited Hongkong",
  "poc" : "Richard Chan",
  "email" : "richard.chan@kuehne-nagel.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 12,
  "name" : "Kuehne & Nagel Limited.",
  "poc" : "Matias,hihping Liou",
  "email" : "shihping.liou@kuehne-nagel.com, Matias.Salazar@kuehne-nagel.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 13,
  "name" : "Kuehne + Nagel (Asia Pacific) Management Pte Ltd",
  "poc" : "Prabhu, Casper",
  "email" : "n.prabhu@kuehne-nagel.com, casper.ellerbaek@kuehne-nagel.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 14,
  "name" : "Kuehne + Nagel (AG & Co.) KG - Hamburg",
  "poc" : "Matias, Markus",
  "email" : "matias.salazar@kuehne-nagel.com",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
}, {
  "id" : 15,
  "name" : "Mashwire Pte Ltd",
  "poc" : "Hon Wui",
  "email" : "honwui@mashwire.com.sg",
  "status" : "ACTIVE",
  "geography" : {
    "id" : 2,
    "code" : "SG"
  }
} ];
