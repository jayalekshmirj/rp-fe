import React, { Component } from 'react';
import { bindActionCreators } from 'redux';

import customer_table from './mock';
import firebase from 'firebase';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AddCustomer from './components/AddCustomer/AddCustomer';
// import RowContent from './components/CustomerProjects/CustomerProjects';
import * as actions from './redux/actions';
import { connect } from 'react-redux';
import './customer.scss';


import {
  Card,
  CardActions,
  CardContent,
  Avatar,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination,
  Button,
  DialogTitle,
  DialogActions,
  DialogContent,
  Dialog,

} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';

import { SearchInput } from '../../../components';

class Customers extends Component {

  static propTypes = {
    customers: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      totalPage: 0,
      isAddCustomer: false,
      pageLimit: 6,
      isDeleteModalOpen: false,
      editCustomerData: {},
      addCustomerActionType: 'add',
      deleteIndex: -1,
      customersData: [],
      updatedCustomerData: {},
      deleteId: '',
      status:'',
      geography:'',
      customerToDelete:{
        index:'',
        id:''
      },
    };
  }

  componentDidMount() {
    let { retrieveCustomers } = this.props.actions;
    console.log(retrieveCustomers, 'retrieveCustomers');
    retrieveCustomers();
    let totalPage = this.state;
    let { totalElements } = this.props.customers;
    // totalPage = Math.ceil(totalElements / this.state.pageLimit);
    const { currentPage, pageLimit } = this.state;
    retrieveCustomers({ page: currentPage, pageLimit });
  }

  
  removeCustomer = (customerToDelete) => {
    this.setState({ isDeleteModalOpen: true, customerToDelete: customerToDelete });
  };

  toggleModal = () => {
    this.setState({ isDeleteModalOpen: false });
  };

  deleteCustomer = () => {
    const { deleteCustomer,retrieveCustomers } = this.props.actions;
    const { customerToDelete,currentPage, pageLimit  } = this.state;
    deleteCustomer(customerToDelete.id);
    this.setState({ isDeleteModalOpen: false });
    retrieveCustomers({page:currentPage,pageLimit})
  };


  editCustomer = customer => {
    this.setState(
      {
        addCustomerActionType: 'edit',
        editCustomerData: customer,
      },
      () =>
        this.setState({
          isAddCustomer: true,
        }),
    );
  };

  addEditModalAction = customersData => {
    const { createCustomers, updateCustomers,retrieveCustomers } = this.props.actions;
    console.log(createCustomers, 'createCustomers first check');

    const { addCustomerActionType, editCustomerData,currentPage, pageLimit  } = this.state;
    if (addCustomerActionType === 'add') {
      createCustomers(customersData);
      retrieveCustomers({ page: currentPage, pageLimit });
    } else {
      updateCustomers(customersData);
      retrieveCustomers({ page: currentPage, pageLimit });
    }
  };

  showAddCustomer = () => {
    this.setState(prevState => {
      return {
        isAddCustomer: !prevState.isAddCustomer,
        addCustomerActionType: 'add',
      };
    });
  };

  toggleRow = (e)=> {
    this.setState({open: !this.state.open});
  }
  
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.customers.allCustomers && nextProps.customers.allCustomers.length) {
      this.setState({
        totalPage: Math.ceil(nextProps.customers.totalElements / this.state.pageLimit),
      });
    }
  }

  render() {
    const { isAddCustomer, isDeleteModalOpen, customerToDelete } = this.state;
    let { allCustomers, totalElements, deleteCustomerBegin } = this.props.customers;
    console.log(allCustomers, 'allCustomers to check');
    console.log(totalElements, 'totalElements');
    let { retrieveCustomers } = this.props.actions;

      
    const tableRows = (customer, index)=>{
      return (
      <React.Fragment>
        <TableRow onClick={()=>{this.toggleRow()}} key={customer.id}>
                      <TableCell>{customer.name}</TableCell>
                      <TableCell>{customer.email}</TableCell>
                      <TableCell>{customer.poc}</TableCell>
                      <TableCell>{customer.status}</TableCell>
                      <TableCell className="items-center">
                        <Button variant="contained" className="btn edit">Edit</Button>
                        <Button variant="contained" className="btn btn-danger deleteButton"
                        icon="trash"
                          onClick={() => this.removeCustomer({index:index, id:customer.id})}>Delete</Button>
                      </TableCell>
                    </TableRow>
                    </React.Fragment>


      );
    }
    
    const { currentPage } = this.state;
    return (
      <div className="customers-container">
      <div className="row">
        <span className="flex" />
        <Button
          color="primary"
          variant="contained"
          onClick={() => this.showAddCustomer()}
        >
          Add Customer
        </Button>
      </div>
        <div className="row">
          {/* <SearchInput 
          className="search-input" 
          placeholder="Search Customer"
          /> */}
          <Autocomplete
          className="search-input" 
          placeholder="Search Customer"
          freeSolo
          id="free-solo-2-demo"
          disableClearable
          options={allCustomers.map(option => option.name)}
          renderInput={params => (
          <TextField
            {...params}
            label="Search input"
            margin="normal"
            variant="outlined"
            fullWidth
            InputPsrops={{ ...params.InputProps, type: 'search' }}
            className="dis"
          />
        )}
      />
          {/* <this.PaginationControl /> */}
          </div>
          <CardActions className="actions">
          <TablePagination
          component="div"
          count={totalElements} 
          page={currentPage}
          onChangePage={this.handlePageChange}
          onChangeRowsPerPage={this.handleRowsPerPageChange}
          rowsPerPage={10}
          rowsPerPageOptions={[5, 10, 25]}
        />
          </CardActions>

          
        <div className="customer-table">
          <Card>
            <CardContent className="card-content">
              <div className="inner">
              <Table>
            <TableHead>
              <TableRow>
                <TableCell>Customer Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>POC</TableCell>
                <TableCell>Status</TableCell>
                <TableCell className="items-center">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {allCustomers
                ? allCustomers.map((customer, index) => (
                  tableRows(customer, index)
                )): ''}
            </TableBody>
          </Table>
              </div>
            
            </CardContent>
            <CardActions className="actions">
            <TablePagination
              component="div"
              count={totalElements} 
              page={currentPage}
              onChangePage={this.handlePageChange}
              onChangeRowsPerPage={this.handleRowsPerPageChange}
              rowsPerPage={10}
              rowsPerPageOptions={[5, 10, 25]}
            />
            </CardActions>
          </Card>
          
          <div className="pagination">
            {/* <this.PaginationControl /> */}
      
          </div>
        </div>
        {isAddCustomer && (
          <AddCustomer
            show={isAddCustomer}
            toggleModal={e => this.setState({ isAddCustomer: e })}
            type={this.state.addCustomerActionType}
            createCustomers={e => this.addEditModalAction(e)}
          />
        )}

        {isDeleteModalOpen && (

          <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      // onEntering={handleEntering}
      aria-labelledby="confirmation-dialog-title"
      open={isDeleteModalOpen}
    >
      <DialogTitle id="confirmation-dialog-title">Phone Ringtone</DialogTitle>
      <DialogContent dividers>
        Hiii
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={()=>{alert("ok")}} color="primary">
          Cancel
        </Button>
        <Button onClick={this.deleteCustomer} color="primary">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
          /** <DeleteConfirmation
            show={isDeleteModalOpen}
            title="Delete Customer"
            // disabled={deleteTopicBegin}
            message={
              deleteIndex !== -1
                ? `You are deleting <strong>'${allCustomers[deleteIndex].name}'</strong>. This action cannot be undone. Proceed?`
                : ''
            }
            toggleModal={() => this.toggleModal()}
            confirmDelete={() => this.deleteCustomer()}
          />**/
        )}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    customers: state.customers,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Customers);

