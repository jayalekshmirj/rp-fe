import { Customers } from './';

export default {
  path: '/dashboard/customers',
  name: 'Customers',
  childRoutes: [{ path: 'customers', name: 'Customers', component: Customers, isIndex: true }],
};
