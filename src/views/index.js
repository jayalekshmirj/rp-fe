
export { default as Dashboard } from './Dashboard';
export { default as Icons } from './Icons';
export { default as NotFound } from './NotFound';
// export { default as ProductList } from './ProductList';
// export { default as Settings } from './Settings';
export { default as SignIn } from './SignIn';
export { default as SignUp } from './SignUp';
export {default as Customers} from './Features/customers/Customers';
export {default as Employees} from './Features/employees/Employees';
export {default as Projects} from './Features/projects/Projects';
export {default as Allocation} from './Features/allocation/Allocation';

