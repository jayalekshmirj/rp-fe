import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
// import homeReducer from '../features/home/redux/reducer';
// import commonReducer from '../features/common/redux/reducer';
// import projectsReducer from '../features/projects/redux/reducer';
import customerReducer from '../views/Features/customers/redux/reducer';
// import employeeReducer from '../features/employees/redux/reducer';

const reducerMap = {
  router: routerReducer,
  // home: homeReducer,
  // common: commonReducer,
  // projects: projectsReducer,
  customers: customerReducer,
  // employees: employeeReducer,
};

export default combineReducers(reducerMap);
