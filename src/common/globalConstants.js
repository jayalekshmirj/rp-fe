export const apiEndPoints = {
  customers: {
    RETRIEVE_CUSTOMERS: 'customers',
    CREATE_CUSTOMERS: 'customers',
  },

  projects: {
    RETRIEVE_PROJECTS: 'projects',
    CREATE_PROJECTS: 'projects',
  },
  employees: {
    RETRIEVE_EMPLOYEES: 'employees',
    CREATE_EMPLOYEES: 'employees',
    RETRIEVE_TECHNOLOGY: 'job-levels',
  },
};
