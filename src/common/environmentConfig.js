const config = {
  development: {
    BASE_URL: 'http://resourceplanner-api.perfomatix.online/api/',
  },
};

module.exports = config;
