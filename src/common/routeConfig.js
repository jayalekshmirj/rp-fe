import { App } from '../features/home';
import { PageNotFound } from '../features/common';
import homeRoute from '../features/home/route';
import commonRoute from '../features/common/route';
import _ from 'lodash';
import employeeRoute from '../features/employees/route';
import projectRoute from '../features/projects/route';
import customerRoute from '../features/customers/route';
import allocationRoute from '../features/allocation/route';
import Login from '../features/login/Login';
import loginRoute from '../features/login/route';
// import sourcesRoute from '../features/sources/route';
// import reportsRoute from '../features/reports/route';
// const childRoutes = [homeRoute, commonRoute, employeeRoute, sourcesRoute, reportsRoute];
const childRoutes = [
  homeRoute,
  commonRoute,
  employeeRoute,
  projectRoute,
  customerRoute,
  allocationRoute,
];

const routes = [
  {
    path: '/login',
    component: Login,
    childRoutes: [...loginRoute],
  },
  {
    path: '/',
    component: App,
    childRoutes: [
      ...childRoutes,
      { path: '*', name: 'Page not found', component: PageNotFound },
    ].filter(r => r.component || (r.childRoutes && r.childRoutes.length > 0)),
  },
];

function handleIndexRoute(route) {
  if (!route.childRoutes || !route.childRoutes.length) {
    return;
  }

  const indexRoute = _.find(route.childRoutes, child => child.isIndex);
  if (indexRoute) {
    const first = { ...indexRoute };
    first.path = '';
    first.exact = true;
    first.autoIndexRoute = true;
    route.childRoutes.unshift(first);
  }
  route.childRoutes.forEach(handleIndexRoute);
}

routes.forEach(handleIndexRoute);
export default routes;
