import axios from 'axios';
import Promise from 'promise';
import config from './environmentConfig';
import { toast } from 'react-toastify';
const env = process.env.NODE_ENV || 'development';
const http = axios.create({
  baseURL: config[env].BASE_URL,
});

http.interceptors.request.use(
  function(config) {
    // const token = store.getState().session.token;
    const token = `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU4MDgxOTgzN30.VgFtkp_ROEynhcCLHVZmv0hGSKujfqfaLwdX5Ggh6uFyj4o9VoCDc6ZApf3I3FRMOCuR_mL4M7o0Jxw-4JHusQ`;
    config.headers.Authorization = token;
    return config;
  },
  function(error) {
    return Promise.resolve({ error });
  },
);
http.interceptors.response.use(
  response => {
    return response;
  },
  err => {
    return new Promise(function(resolve, reject) {
      toast.error('Something went wrong.');
      if (err.response && err.response.status === 401) {
        return false;
      }
      throw err;
    });
  },
);

export default http;
